import { Component, OnInit } from '@angular/core';
import { NavController} from '@ionic/angular';

export class AppUtil {

  constructor(public navCtrl: NavController) { }

  public openPage(pageName: string){
     //this.router.navigate(['tabs']);
    this.navCtrl.navigateRoot(pageName);
  }

  public deslogar(){
    this.navCtrl.navigateRoot("/login");
  }

}
