import { AppUtil } from './../app-util';
import { Component, OnInit } from '@angular/core';
import { NavController} from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends AppUtil {

  autenticado: boolean;
  email: string;
  senha: string;

  constructor(public navCtrl: NavController, public router: Router){
    super(navCtrl);
  }

  public cadastrar(){

  }

  public logar(){
    super.openPage("/tabs");
  }

  private autenticar(){

  }


}
